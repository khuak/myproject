const taskInstance =axios.create({
  baseURL:"https://dev-api.built.io/v1/classes/todolist/objects/",
  headers : {
   'application_api_key':'bltaeeae8cebd7d7387',
   'authtoken':`${document.cookie.split("=")[1]}`
  }
});

const logoutInstance =axios.create({
  baseURL:`https://dev-api.built.io/v1/application/users/logout/`,
  headers : {
    'application_api_key':'bltaeeae8cebd7d7387',
    'authtoken':`${document.cookie.split("=")[1]}`
   }
})
const userInstance = axios.create({
  baseURL:"https://dev-api.built.io/v1/application/users/current",
    headers : {
       'application_api_key':'bltaeeae8cebd7d7387',
       'authtoken':`${document.cookie.split("=")[1]}`
      }
 })
  

const updateDOM = (event) => {
  event.preventDefault();
  var task = document.forms.todo.task.value;
  addTask(task);
}

let taskid;
const addTask =(task)=>{
  var reminderList = document.getElementById('tasklist');
  var listitem = document.createElement('li');  
    taskInstance.post('',{
      "object":{
        "task":`${task}`,
        "status":"incomplete"
      }
    }).then((response)=>{
      resolve(response);
      taskid =response.data.objects.uid;
    }).catch((error)=>{
      reject(error);
    })
    listitem.setAttribute('id',taskid);
    reminderList.appendChild(listitem);  
    listitem.innerHTML = task+''+`<button onclick="completeTask(event)"type="button">Done</button>` + 
                             `<button onclick="deleteTask(event)"type="button">Delete</button>` +
                             `<button onclick="shareTask(event)"type="button">Share</button>`;       

}
  
const deleteTask =(event)=>{
  var taskId = event.target.parentNode.id;
  taskInstance.delete(taskId)
  .then((response) => {
    console.log(response);
    var ul = event.target.parentNode.parentNode;
    var li = event.target.parentNode;
    ul.removeChild(li);
   }).catch((err) => {
     console.log(err);
   });

}

const completeTask =()=>{
  var taskId = event.target.parentNode.id;
  taskInstance.put(taskId,{
    "object":{
      "status":"complete"
    }
  }).then((response)=>{
     console.log(response);
     strik(event);
  }).catch((error)=>{
    console.log(error);
  })
}

const strik =(event)=>{
  event.target.parentNode.setAttribute('style','text-decoration:line-through');
}

const getCurrUserTask =()=>{
  return new Promise((resolve,reject)=>{
    userInstance.get()
     .then((response)=>{
       resolve(response);
      }).catch((error)=>{
       reject(error);
      })
   })
}

const logout =()=>{
  logoutInstance.delete()
  .then((response)=>{
    console.log(response);
    document.cookie ='authtoken=';
  window.location.href = "../html/registration.html";
  }).catch((error)=>{
    console.log(error);
  })
}

window.onload =()=>{
  getCurrUserTask()
  .then((response)=>{
    taskInstance.post('',{
     "_method":"get",
     "query":{
        "app_user_object_uid":`${response.data.application_user.uid}`
      }
    }).then((response)=>{
      console.log(response);
      response.data.objects.forEach((i)=>{
      let task = i.task;
      var reminderList = document.getElementById('tasklist');
      var listitem = document.createElement('li');
      listitem.setAttribute('id',i.uid);
      if(i.status=="complete"){
       reminderList.appendChild(listitem).innerHTML = task+`<button onclick="completeTask(event)"type="button">Done</button>` + 
                                                   `<button onclick="deleteTask(event)"type="button">Delete</button>`+
                                                   `<button onclick="shareTask(event)"type="button">Share</button>`;
       listitem.setAttribute('style','text-decoration:line-through');
       }else{
        reminderList.appendChild(listitem).innerHTML = task+`<button onclick="completeTask(event)"type="button">Done</button>` + 
                                                     `<button onclick="deleteTask(event)"type="button">Delete</button>`+
                                                     `<button onclick="shareTask(event)"type="button">Share</button>`;
       }
      })
    }).catch((error)=>{
     console.log(error);
    })
  }).catch((error)=>{
    console.log(error);
  })
}

