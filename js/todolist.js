const taskInstance = axios.create({
  baseURL: "https://dev-api.built.io/v1/classes/todolist/objects/",
  headers: {
    'application_api_key': 'bltaeeae8cebd7d7387',
    'authtoken': `${document.cookie.split("=")[1]}`
  }
});

const logoutInstance = axios.create({
  baseURL: `https://dev-api.built.io/v1/application/users/logout/`,
  headers: {
    'application_api_key': 'bltaeeae8cebd7d7387',
    'authtoken': `${document.cookie.split("=")[1]}`
  }
})
const curr_userInstance = axios.create({
  baseURL: "https://dev-api.built.io/v1/application/users/current",
  headers: {
    'application_api_key': 'bltaeeae8cebd7d7387',
    'authtoken': `${document.cookie.split("=")[1]}`
  }
})

const userInstance = axios.create({
  baseURL: `https://dev-api.built.io/v1/classes/built_io_application_user/objects/`,
  headers: {
    'application_api_key': 'bltaeeae8cebd7d7387',
    'authtoken': `${document.cookie.split("=")[1]}`
  }

})

const updateDOM = (event) => {
  event.preventDefault();
  var task = document.forms.todo.task.value;
  addTask(task);
}

let taskid;
const addTask = (task) => {
  var reminderList = document.getElementById('tasklist');
  var listitem = document.createElement('li');
  taskInstance.post('', {
    "object": {
      "task": `${task}`,
      "status": "incomplete"
    }
  }).then((response) => {
    resolve(response);
    taskid = response.data.objects.uid;
  }).catch((error) => {
    reject(error);
  })
  listitem.setAttribute('id', taskid);
  reminderList.appendChild(listitem);
  listitem.innerHTML = task + '' + `<button onclick="completeTask(event)"type="button">Done</button>` +
    `<button onclick="deleteTask(event)"type="button">Delete</button>` +
    `<button onclick="createpopUp(event)"type="button">Share</button>`;

}

const completeTask = (event) => {
  var taskId = event.target.parentNode.id;
  taskInstance.put(taskId, {
    "object": {
      "status": "complete"
    }
  }).then((response) => {
    console.log(response);
    strik(event);
  }).catch((error) => {
    console.log(error);
  })
}

const deleteTask = (event) => {
  var taskId = event.target.parentNode.id;
  taskInstance.delete(taskId)
    .then((response) => {
      console.log(response);
      var ul = event.target.parentNode.parentNode;
      var li = event.target.parentNode;
      ul.removeChild(li);
    }).catch((err) => {
      console.log(err);
    });

}

const createpopUp = (event) => {
  //console.log(event.target.parentNode);
  var listitem = event.target.parentNode;
  var div = document.createElement("div");
  userInstance.get()
    .then((user) => {
      console.log(user);
      user.data.objects.forEach((i) => {
        let checkbox = `<div><input type ="checkbox"name="name1"value="${i.uid}">
                   <label for="">${i.email}</label>
                   </div>`;
        div.innerHTML += checkbox;
      })
      listitem.appendChild(div);
      let button = `<br><button onclick ="updateCollaborators(event)">OK</button>`;
      div.innerHTML += button;
      listitem.appendChild(div);
    }).catch((error) => {
      console.log(error);
    })
}

const updateCollaborators = (event) => {
  //console.log(event.target.parentNode.parentNode);
  var taskId = event.target.parentNode.parentNode.id;
  const collaborator = [];
  let children = event.target.parentNode.childNodes;
  children.forEach((el) => {
    el.childNodes.forEach((child) => {
      if (child.checked) {
        if (!collaborator.includes(child.value)) {
          collaborator.push(child.value);
        }
      }
    })
  })
  taskInstance.put(taskId, {
    "object": {
      "collaborators": collaborator
    }
  }).then((response) => {
    console.log(response);
  }).catch((error) => {
    console.log(error);
  })
}
//collaborator.push(child.value)
const strik = (event) => {
  event.target.parentNode.setAttribute('style', 'text-decoration:line-through');
}

const getCurrUserTask = () => {
  return new Promise((resolve, reject) => {
    curr_userInstance.get()
      .then((response) => {
        resolve(response);
      }).catch((error) => {
        reject(error);
      })
  })
}

const logout = () => {
  logoutInstance.delete()
    .then((response) => {
      console.log(response);
      document.cookie = 'authtoken=';
      window.location.href = "../html/registration.html";
    }).catch((error) => {
      console.log(error);
    })
}

window.onload = () => {
  getCurrUserTask()
    .then((response) => {
      taskInstance.post('', {
        "_method": "get",
        "query": {
          "app_user_object_uid": `${response.data.application_user.uid}`
        }
      }).then((response) => {
        console.log(response);
        response.data.objects.forEach((i) => {
          let task = i.task;
          var reminderList = document.getElementById('tasklist');
          var listitem = document.createElement('li');
          listitem.setAttribute('id', i.uid);
          if (i.status == "complete") {
            reminderList.appendChild(listitem).innerHTML = task + ' ' + `<button onclick="completeTask(event)"type="button">Done</button>` +
              `<button onclick="deleteTask(event)"type="button">Delete</button>` +
              `<button onclick="createpopUp(event)"type="button">Share</button>`;
            listitem.setAttribute('style', 'text-decoration:line-through');
          } else {
            reminderList.appendChild(listitem).innerHTML = task + ' ' + `<button onclick="completeTask(event)"type="button">Done</button>` +
              `<button onclick="deleteTask(event)"type="button">Delete</button>` +
              `<button onclick="createpopUp(event)"type="button">Share</button>`;
          }
        })
      }).catch((error) => {
        console.log(error);
      })
      document.querySelector('p[class="welcome"]').innerHTML += ` ` + `${response.data.application_user.first_name}`;
    }).catch((error) => {
      console.log(error);
    })
}

