const url = "https://dev-api.built.io/v1/classes/todolist/objects/";
let taskid;

const fetchalltask =axios.create({
   baseURL:"https://dev-api.built.io/v1/classes/todolist/objects/",
   headers : {
    'application_api_key':'bltaeeae8cebd7d7387',
    'authtoken':`${document.cookie.split("=")[1]}`
   }
});

const updateDOM = (event) => {
  event.preventDefault();
  var task = document.forms.todo.task.value;
  addTask(task);
}

addTask = (task) => {
  var reminderList = document.getElementsByClassName('tasklist');
  var listitem = document.createElement('li');
     
  axios.post(url,{ 
     "object": {
       "task": task,
       "status": "incomplete"
      }
    },{
      headers : {
        'application_api_key':'bltaeeae8cebd7d7387',
        'authtoken':`${document.cookie.split("=")[1]}`
       }
      }
  ).then((response) => {
    taskid = response.data.object.uid;       
    }).catch((err) => {
      console.log(err);
    });
    listitem.setAttribute('id',taskid);
    reminderList.appendChild(listitem);  
    listitem.innerHTML = task+`<button onclick="completeTask(event)"type="button">Done</button>` + 
                             `<button onclick="deleteTask(event)"type="button">Delete</button>` +
                             `<button onclick="shareTask(event)"type="button">Share</button>`;   
}

const deleteTask = (event) => {
  var taskId = event.target.parentNode.id;

 axios.delete(url+taskId,{
    headers : {
      'application_api_key':'bltaeeae8cebd7d7387',
      'authtoken':`${document.cookie.split("=")[1]}`
     }
    })
    .then((response) => {
      console.log(response);
      var ul = event.target.parentNode.parentNode;
      var li = event.target.parentNode;
      ul.removeChild(li);
   
    }).catch((err) => {
      console.log(err);
    });

 }

 const completeTask =(event)=>{
   var taskId = event.target.parentNode.id;
   console.log(taskId);
   axios.put(url+taskId,{
     "object":{
       "status":"complete"
      }
     },{
      headers : {
        'application_api_key':'bltaeeae8cebd7d7387',
        'authtoken':`${document.cookie.split("=")[1]}`         
       }
      }
   ).then((response)=>{
     console.log(response);
     strik(event);
   }).catch((error)=>{
     console.log(error);
   })
 }

 const strik =(event)=>{
   event.target.parentNode.setAttribute('style','text-decoration:line-through');  
 }

 const getUser = ()=>{
  const userfetch = axios.create({
    baseURL:"https://dev-api.built.io/v1/application/users/current",
    headers : {
        'application_api_key':'bltaeeae8cebd7d7387',
         'authtoken':`${document.cookie.split("=")[1]}`
      }
    })
  return new Promise((resolve,reject)=>{
   userfetch.get()
    .then((response)=>{
      resolve(response);
     }).catch((error)=>{
      reject(error);
     })
  })
}

window.onload = () => {  
  var reminderList = document.getElementById('tasklist');
  getUser()
  .then((response)=>{
   fetchalltask.post('',{
      "_method":"get",
       "query":{
        "app_user_object_uid":`${response.data.application_user.uid}`
       }
    }).then((data)=>{
      data.data.objects.forEach((i)=>{
      let task = i.task;
      var listitem = document.createElement('li');
      listitem.setAttribute('id',i.uid);
      if(i.status=="complete"){
        reminderList.appendChild(listitem).innerHTML = task+`<button onclick="completeTask(event)"type="button">Done</button>` + 
                                                   `<button onclick="deleteTask(event)"type="button">Delete</button>`;
        listitem.setAttribute('style','text-decoration:line-through');
       }else{
        reminderList.appendChild(listitem).innerHTML = task+`<button onclick="completeTask(event)"type="button">Done</button>` + `<button onclick="deleteTask(event)"type="button">Delete</button>`;
       }
      })
     }).catch((error)=>{
      console.log(error);
   })
  }).catch((error)=>{
    console.log(error);
  })

  
    
  
}
  
const logout =()=>{
  var url = "https://dev-api.built.io/v1/application/users/logout/";
  axios.delete(url,{
      headers : {
     'application_api_key':'bltaeeae8cebd7d7387',
     'authtoken':`${document.cookie.split("=")[1]}`
     }
    }
  ).then((response)=>{
    //console.log(response);
    document.cookie ='authtoken=';
    window.location.href = "../html/registration.html";
  }).catch((error)=>{
    console.log(error);
  }) 
  
}




