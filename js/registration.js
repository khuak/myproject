const registerInstance =axios.create({
    baseURL :`https://dev-api.built.io/v1/application/users/`,
    headers:{
      'application_api_key':'bltaeeae8cebd7d7387'
     }    
})

const validateform =(event)=>{
    event.preventDefault();
    let email =document.forms.userform.email.value;
    let firstName = document.forms.userform.firstName.value;
    let lastName = document.forms.userform.lastName.value;
    let password = document.forms.userform.password.value;
    let confirmPassword =document.forms.userform.confirmPassword.value;
    validateEmail(email);
    validateFirstName(firstName);
    validateLastName(lastName);
    validatePassword(password,confirmPassword);
    addUser(email,firstName,lastName,password,confirmPassword);
 }

 const addUser =(email,firstName,lastName,password,confirmPassword)=>{
    registerInstance.post('',{
        "application_user":{
        "email":`${email}`,
        "first_name":`${firstName}`,
        "last_name":`${lastName}`,
        "password":`${password}`,
        "password_confirmation":`${confirmPassword}`
        }
      }).then((response)=>{
        console.log(response);application
        alert("you have successfully registered");
        window.location.href="http://127.0.0.1:5500/html/login.html";
      }).catch((err)=>{
        console.log(err);
      });
      
 }

const validateEmail =(email)=>{
    let re = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    if(!re.test(email)){
        document.getElementById('erroremail').innerHTML="invalid email";
        return false;
    }
    else{
        document.getElementById('erroremail').innerHTML="";
    }
    return true;
}

const validateFirstName =(firstName)=>{
    let re = /^[a-zA-Z ]{2,80}$/;  
    if(!re.test(firstName)){
        firstName='';
        document.getElementById('errorfname').innerHTML="invalid first name";
        return false;
    }else{
        document.getElementById('errorfname').innerHTML="";
    }
    return true;
}
const validateLastName =(lastName)=>{
    let re = /^[a-zA-Z ]{2,80}$/;
    if(!re.test(lastName)){
        lastName='';
        document.getElementById('errorlname').innerHTML="invalid last name";
        return false;
    }else{
        document.getElementById('errorlname').innerHTML="";
    }
    return true;

}

const validatePassword =(password,confirmPassword)=>{
    if(!(password===confirmPassword)){
        password.value='';
        confirmPassword.value='';   
        document.getElementById('errorcpass').innerHTML="password does not match";
        return false;
    }else{
        document.getElementById('errorcpass').innerHTML="";
    }
    return true;
}
